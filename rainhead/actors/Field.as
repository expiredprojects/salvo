package rainhead.actors 
{
	import rainhead.utils.Atlas;
	import starling.display.Button;
	import starling.textures.Texture;
	
	public class Field extends Button 
	{
		public static const UNKNOWN:String = "unknown";
		public static const AVAILABLE:String = "available";
		
		private var type:String = Field.UNKNOWN;
		public var ix:int;
		public var iy:int;
		public var guests:Array = new Array();
		
		public function Field(_texture:Texture, _ix:int, _iy:int) 
		{			
			super(_texture);
			this.pivotX = this.width >> 1;
			this.pivotY = this.height >> 1;
			
			ix = _ix;
			iy = _iy;
		}
		
		public function setStatus(_type:String):void 
		{
			type = _type;
			this.changeTexture(Atlas.getTexture(type));
		}
		
		public function get status():String 
		{
			return type;
		}
	}
}