package rainhead.actors 
{
	import rainhead.utils.Atlas;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.textures.Texture;
	
	public class Board extends Sprite 
	{
		public var arrayBoard:Array;
		
		public function Board() 
		{
			addEventListener(Event.ADDED_TO_STAGE, initSelf);
		}	
		
		private function initSelf(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, initSelf);
			
			var fieldTexture:Texture = Atlas.getTexture(Field.UNKNOWN);
			arrayBoard = new Array();
			
			for (var i:int = 0; i < 10; i++) 
			{
				arrayBoard[i] = new Array();
				for (var j:int = 0; j < 10; j++) 
				{
					arrayBoard[i].push(new Field(fieldTexture, j, i));
					arrayBoard[i][j].x = j * (Atlas.SIZE_FACTOR + 1);
					arrayBoard[i][j].y = i * (Atlas.SIZE_FACTOR + 1);
					addChild(arrayBoard[i][j]);
				}
			}
		}
		
		public function cleanSelf():void 
		{
			var tempField:Field;
			
			for (var i:int = 0; i < 10; i++) 
			{
				for (var j:int = 0; j < 10; j++) 
				{
					tempField = arrayBoard[i][j] as Field;
					if (!tempField.enabled) 
					{
						tempField.guests.splice(0, tempField.guests.length);
						tempField.enabled = true;
						
						if (tempField.status != Field.UNKNOWN) 
						{
							tempField.setStatus(Field.UNKNOWN);
						}
					}
				}
			}
		}
	}
}