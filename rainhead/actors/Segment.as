package rainhead.actors 
{
	import starling.display.Button;
	import starling.textures.Texture;
	
	public class Segment extends Button 
	{
		public static const ALIVE:String = "alive";
		public static const HIT:String = "hit";
		public static const DEAD:String = "dead";
		
		public var ix:int = 11;
		public var iy:int = 11;
		
		public function Segment(_texture:Texture)
		{
			super(_texture);
			this.pivotX = this.width >> 1;
			this.pivotY = this.height >> 1;
		}
	}
}