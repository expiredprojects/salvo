package rainhead.actors 
{
	import rainhead.utils.Atlas;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.textures.Texture;
	
	public class Ship extends Sprite 
	{
		public var length:int;
		private var arrayShip:Array;
		
		public function Ship(i:int)
		{
			length = i;
			addEventListener(Event.ADDED_TO_STAGE, initSelf);
		}
		
		private function initSelf(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, initSelf);
			
			var segmentTexture:Texture = Atlas.getTexture(Segment.ALIVE);
			arrayShip = new Array();
			
			for (var i:int = 0; i < length; i++) 
			{
				arrayShip.push(new Segment(segmentTexture));
				arrayShip[i].x = i * (Atlas.SIZE_FACTOR + 1);
				addChild(arrayShip[i]);
			}
		}
		
		public function cleanSelf():void 
		{
			var tempSegment:Segment;
			
			for (var i:int = 0; i < length; i++) 
			{
				tempSegment = arrayShip[i] as Segment;
				tempSegment.ix = 11;
				tempSegment.iy = 11;
			}
		}
	}
}