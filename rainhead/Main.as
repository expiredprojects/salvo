package rainhead
{
	import flash.display.Sprite;
	import flash.events.Event;
	import starling.core.Starling;
	
	public class Main extends Sprite 
	{
		private var starling:Starling;
		
		public function Main()
		{
			if (stage) initSelf();
			else addEventListener(Event.ADDED_TO_STAGE, initSelf);
		}
		
		private function initSelf(e:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, initSelf);
			
			starling = new Starling(Game, this.stage);
			starling.start();
			starling.showStatsAt();
		}
	}
}