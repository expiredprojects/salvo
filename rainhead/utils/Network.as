package rainhead.utils 
{
	import com.smartfoxserver.v2.core.SFSEvent;
	import com.smartfoxserver.v2.requests.LoginRequest;
	import com.smartfoxserver.v2.SmartFox;
	import rainhead.events.game.Response;
	import rainhead.events.rps.Roll;
	import rainhead.events.rps.Rolled;
	import rainhead.events.welcome.Logged;
	import starling.core.Starling;
	import starling.display.Sprite;
	
	public class Network extends Sprite
	{
		private var smartFox:SmartFox;
		private var username:String;
		
		public function Network() 
		{
			smartFox = new SmartFox();
			trace(smartFox.version);
			smartFox.addEventListener(SFSEvent.CONFIG_LOAD_SUCCESS, onLoad);
			smartFox.addEventListener(SFSEvent.CONFIG_LOAD_FAILURE, onFailure);
			smartFox.addEventListener(SFSEvent.CONNECTION, handleConnect);
			smartFox.addEventListener(SFSEvent.CONNECTION_LOST, handleLost);
		}
		
		public function invokeConnect(_username:String):void 
		{
			username = _username;
			smartFox.loadConfig();
		}
		
		private function onLoad(e:SFSEvent):void 
		{
			trace(smartFox.config.host + ":" + smartFox.config.port);
		}
		
		private function onFailure(e:SFSEvent):void 
		{
			trace("Loading failure!");
		}
		
		private function handleConnect(e:SFSEvent):void 
		{
			if (e.params.success) 
			{
				smartFox.addEventListener(SFSEvent.LOGIN, handleLogin);
				smartFox.addEventListener(SFSEvent.LOGIN_ERROR, handleError);
				trace("Online: " + smartFox.isConnected);
				smartFox.send(new LoginRequest(username, "ok", "Salvation"));				
			}
			else 
			{
				trace(e.params.errorMessage);
				this.dispatchEvent(new Logged(Logged.OFFLINE));
			}
		}
		
		private function handleLost(e:SFSEvent):void 
		{
			trace(e.params.reason);
		}
		
		private function handleLogin(e:SFSEvent):void 
		{
			this.dispatchEvent(new Logged(Logged.ONLINE));
			trace(e.params.user.name);
		}
		
		private function handleError(e:SFSEvent):void 
		{
			trace(e.params.errorMessage);
		}
		
		public function invokeDisconnect():void 
		{
			smartFox.disconnect();
		}
		
		public function sendReady():void 
		{
			//sendReady()->
			//listenReady()->
			handleReady();
		}
		
		private function handleReady():void //Server code -> Opponent is Ready
		{
			this.dispatchEvent(new Response(Response.OPPONENT_READY));
		}
		
		public function sendRoll(roll:String):void
		{
			//sendRoll(roll)->
			//listenRolled(rolled)->
			handleRolled(roll); 
		}
		
		private function handleRolled(rolled:String):void //Server code -> Opponent is ROCK
		{
			if (rolled == Roll.ROCK)
			{
				this.dispatchEvent(new Rolled(Rolled.DRAW));
			}
			else if (rolled == Roll.PAPER) 
			{
				this.dispatchEvent(new Rolled(Rolled.WON));
			}
			else if (rolled == Roll.SCISSORS) 
			{
				this.dispatchEvent(new Rolled(Rolled.LOST));
			}
		}
	}
}