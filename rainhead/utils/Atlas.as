package rainhead.utils 
{
	import flash.utils.Dictionary;
	import rainhead.actors.Field;
	import rainhead.actors.Segment;
	import rainhead.ui.Mover;
	import rainhead.ui.Typo;
	import starling.textures.Texture;
	
	public class Atlas 
	{	
		public static const SIZE_FACTOR:int = 37;
		private static var textureCache:Dictionary;
		
		public function Atlas() 
		{
			
		}
		
		public static function getTexture(type:String):Texture
		{			
			if (textureCache == null) 
			{
				textureCache = new Dictionary();
			}
			
			if (textureCache[type] == undefined) 
			{
				defineCache(type);
			}
			
			return textureCache[type];
		}
		
		static private function defineCache(type:String):void 
		{
			if (type == Field.AVAILABLE) 
			{
				textureCache[type] = Texture.fromColor(SIZE_FACTOR, SIZE_FACTOR, 0xffCCFE80);
			}
			else if (type == Segment.ALIVE)
			{
				textureCache[type] = Texture.fromColor(SIZE_FACTOR, SIZE_FACTOR, 0xafFEBFDC);
			}
			else if (type == Mover.LOGIN) 
			{
				textureCache[type] = Texture.fromColor(50, 50);
			}
			else if (type == Typo.LOGIN)
			{
				textureCache[type] = Texture.fromColor(300, 50);
			}
			else 
			{
				textureCache[type] =  Texture.fromColor(SIZE_FACTOR, SIZE_FACTOR);
			}
		}
	}
}