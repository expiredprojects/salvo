package rainhead.utils 
{
	public class Location 
	{
		public var x:int;
		public var y:int;
		
		public function Location(_x:int, _y:int) 
		{
			x = _x;
			y = _y;
		}
	}
}