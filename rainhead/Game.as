package rainhead 
{
	import rainhead.events.game.Action;
	import rainhead.events.game.Response;
	import rainhead.events.lobby.Request;
	import rainhead.events.rps.Roll;
	import rainhead.events.rps.Rolled;
	import rainhead.events.welcome.Logged;
	import rainhead.events.welcome.Login;
	import rainhead.screens.GameScreen;
	import rainhead.screens.WelcomeScreen;
	import rainhead.utils.Network;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	
	public class Game extends Sprite 
	{
		private var canvas:Quad;
		private var welcomeScreen:WelcomeScreen;
		private var gameScreen:GameScreen;
		private var network:Network;
		
		public function Game() 
		{
			this.addEventListener(Event.ADDED_TO_STAGE, initSelf);
		}
		
		private function initSelf(e:Event):void 
		{
			this.removeEventListener(Event.ADDED_TO_STAGE, initSelf);
			
			network = new Network();
			addChild(network);
			
			canvas = new Quad(800, 600, 0xf2f3b3);
			addChild(canvas);
			
			gameScreen = new GameScreen();
			addChild(gameScreen);
			gameScreen.disposeGame();
			
			welcomeScreen = new WelcomeScreen();
			addChild(welcomeScreen);			
			welcomeScreen.initWelcome();
			
			this.addEventListener(Login.LOGIN, onLogin);
		}	
		
		private function onLogin(e:Login):void 
		{
			this.removeEventListener(Login.LOGIN, onLogin);
			this.addEventListener(Logged.LOGGED, onLogged);
			
			network.invokeConnect(String(e.result));
		}
		
		private function onLogged(e:Logged):void 
		{
			this.removeEventListener(Logged.LOGGED, onLogged);
			
			if (e.result == Logged.ONLINE || true) 
			{
				welcomeScreen.disposeWelcome();
				gameScreen.initGame();
				this.addEventListener(Action.ACTION, onAction);
			}
			else
			{
				this.addEventListener(Login.LOGIN, onLogin);
			}
		}
		
		private function onRequest(e:Request):void 
		{
			
		}
		
		private function onAction(e:Action):void 
		{
			if (e.result == Action.READY) 
			{
				this.addEventListener(Response.RESPONSE, onResponse);
				network.sendReady();
			}
			else if (e.result == Action.ABORT)
			{
				network.invokeDisconnect();
				gameScreen.disposeGame();
				welcomeScreen.initWelcome();
				this.addEventListener(Login.LOGIN, onLogin);
			}
		}
		
		private function onResponse(e:Response):void 
		{
			this.removeEventListener(Response.RESPONSE, onResponse);
			
			if (e.result == Response.OPPONENT_READY) 
			{
				gameScreen.intoRoll();
				this.addEventListener(Roll.ROLL, onRoll);
			}
			else if (e.result == Response.ABORT_SUCCESS) 
			{
				//Network callback of Abort code
			}
		}
		
		private function onRoll(e:Roll):void 
		{
			this.removeEventListener(Roll.ROLL, onRoll);
			this.addEventListener(Rolled.ROLLED, onRolled);
			network.sendRoll(e.result as String);
		}
		
		private function onRolled(e:Rolled):void 
		{
			this.removeEventListener(Rolled.ROLLED, onRolled);
			if (e.result == Rolled.DRAW) 
			{
				this.addEventListener(Roll.ROLL, onRoll);
				gameScreen.restartRoll();
			}
			else if (e.result == Rolled.WON || e.result == Rolled.LOST)
			{
				gameScreen.intoBattle(e.result as String);
			}
		}
	}
}