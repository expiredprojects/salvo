package rainhead.screens 
{
	import rainhead.actors.Board;
	import rainhead.actors.Field;
	import rainhead.actors.Segment;
	import rainhead.actors.Ship;
	import rainhead.events.game.Action;
	import rainhead.screens.phase.Bake;
	import rainhead.screens.phase.Battle;
	import rainhead.screens.phase.Deploy;
	import rainhead.screens.phase.RPS;
	import rainhead.ui.Mover;
	import starling.display.Button;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	
	public class GameScreen extends Sprite 
	{
		private var fleet:Array;
		private var playerBoard:Board;
		private var enemyBoard:Board;
		private var deploy:Deploy;
		private var bake:Bake;
		private var rps:RPS;
		private var battle:Battle;
		private var abort:Mover;
		private var ready:Mover;
		private var canvas:Quad;
		
		public function GameScreen() 
		{
			this.addEventListener(Event.ADDED_TO_STAGE, initSelf);
		}	
		
		private function initSelf(e:Event):void 
		{
			this.removeEventListener(Event.ADDED_TO_STAGE, initSelf);
			
			canvas = new Quad(800, 600, 0xf2f3b3);
			addChild(canvas);
			canvas.alpha = 0;
			
			playerBoard = new Board;
			addChild(playerBoard);
			playerBoard.y = 100;
			
			enemyBoard = new Board;
			addChild(enemyBoard);
			enemyBoard.x = 800 - enemyBoard.width;
 			enemyBoard.y = playerBoard.y;
			
			fleet = new Array();
			
			for (var i:int = 0; i < 5; i++) 
			{
				fleet.push(new Ship(i + 1));
				addChild(fleet[i]);
			}
			
			abort = new Mover(Mover.ABORT);
			addChild(abort);
			abort.x = 15;
			abort.y = 500 + fleet[0].height;
			
			ready = new Mover(Mover.READY);
			addChild(ready);
			ready.x = 800 - ready.width - abort.x;
			ready.y = abort.y;
			
			deploy = new Deploy(this, playerBoard);
			
			bake = new Bake(playerBoard, fleet);
			
			rps = new RPS();
			rps.visible = false;
			addChild(rps);
			
			battle = new Battle(this, enemyBoard);
        }
		
		public function initGame():void 
		{			
			initShips();
			cleanShips();
			cleanBoards();
			ready.enabled = true;
			
			this.visible = true;
			this.addEventListener(Event.TRIGGERED, onTrigger);
			
			deploy.beginPhase();
		}
		
		public function disposeGame():void 
		{
			if (!isDeployed()) 
			{
				deploy.endPhase();
			}
			else if (battle.isTurn != 0) 
			{
				battle.endPhase();
			}
			
			this.removeEventListener(Event.TRIGGERED, onTrigger);
			this.visible = false;
		}
		
		private function initShips():void 
		{
			fleet[0].x = 30;
			fleet[0].y = 500;
			fleet[0].visible = true;
			
			fleet[1].x = fleet[0].x + fleet[0].width * 2;
			fleet[1].y = fleet[0].y;
			fleet[1].rotation = 0;
			fleet[1].visible = true;
			
			for (var i:int = 2; i < 5; i++) 
			{
				fleet[i].x = fleet[i - 1].x + fleet[i - 1].width + fleet[0].width;
				fleet[i].y = fleet[i - 1].y;
				fleet[i].rotation = fleet[i - 1].rotation;
				fleet[i].visible = true;
			}
		}
		
		private function cleanShips():void 
		{
			for (var i:int = 0; i < fleet.length; i++) 
			{
				(fleet[i] as Ship).cleanSelf();
			}
		}
		
		private function cleanBoards():void 
		{
			playerBoard.x = (800 - playerBoard.width) >> 1;
			playerBoard.cleanSelf();
			enemyBoard.visible = false;
			enemyBoard.cleanSelf();
		}
		
		private function onTrigger(e:Event):void 
		{
			var buttonClicked:Button = e.target as Button;
			
			if (buttonClicked == abort && !deploy.isTick) 
			{
				this.dispatchEvent(new Action(Action.ABORT));
			}
			else if (buttonClicked == ready && !deploy.isTick && isDeployed()) 
			{	
				ready.enabled = false;
				deploy.endPhase();
				
				this.dispatchEvent(new Action(Action.READY));
			}
		}
		
		private function isDeployed():Boolean 
		{
			for (var i:int = 0; i < fleet.length; i++) 
			{
				if ((fleet[i].getChildAt(0) as Segment).ix == 11) 
				{
					return false;
				}
			}
			
			return true;
		}
		
		public function intoRoll():void 
		{
			boardOffset();			
			enemyBoard.visible = true; //Consider multiple
			
			bake.runPhase();
			rps.beginPhase();
		}
		
		private function boardOffset():void //Based on RPS result
		{
			var offset:int = playerBoard.x - (playerBoard.arrayBoard[0][0] as Field).width;	
			
			playerBoard.x -= offset;
			for (var i:int = 0; i < fleet.length; i++) 
			{
				fleet[i].x -= offset;
			}
		}
		
		public function restartRoll():void 
		{
			rps.restartRPS();
		}
		
		public function intoBattle(rolled:String):void 
		{
			rps.endPhase();
			rps.restartRPS();
			battle.beginPhase(rolled);
		}
	}
}