package rainhead.screens 
{
	import rainhead.ui.Mover;
	import rainhead.events.welcome.Login;
	import rainhead.ui.Typo;
	import starling.core.Starling;
	import starling.display.Button;
	import starling.display.Sprite;
	import starling.events.Event;
	
	public class WelcomeScreen extends Sprite 
	{
		private var click:Mover;
		private var typo:Typo;
		
		public function WelcomeScreen() 
		{
			this.addEventListener(Event.ADDED_TO_STAGE, initSelf);
		}	
		
		private function initSelf(e:Event):void 
		{
			this.removeEventListener(Event.ADDED_TO_STAGE, initSelf);
			
			click = new Mover(Mover.LOGIN);
			click.x = (stage.width >> 1) + 2 * click.width;
			click.y = (stage.height - click.height) >> 1;
			addChild(click);
			
			typo = new Typo(200, 50, click);
			typo.x = click.x - typo.width - 50;
			typo.y = click.y;
			addChild(typo);
		}
		
		public function initWelcome():void 
		{
			this.visible = true;
			
			typo.initTypo();
			this.addEventListener(Event.TRIGGERED, onTrigger);
		}
		
		public function disposeWelcome():void 
		{
			this.removeEventListener(Event.TRIGGERED, onTrigger);
			typo.disposeTypo();
			
			this.visible = false;
		}
		
		private function onTrigger(e:Event):void 
		{
			var buttonClicked:Button = e.target as Button;
			if ((buttonClicked == click) && (typo.text != ""))
			{
				Starling.current.nativeStage.focus = null;
				this.dispatchEvent(new Login(typo.text));
			}
		}
	}
}