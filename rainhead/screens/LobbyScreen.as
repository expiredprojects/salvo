package rainhead.screens 
{
	import starling.display.Sprite;
	import starling.events.Event;
	
	public class LobbyScreen extends Sprite 
	{
		
		public function LobbyScreen() 
		{
			this.addEventListener(Event.ADDED_TO_STAGE, initSelf);
		}
		
		private function initSelf(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, initSelf);
			
		}
		
	}

}