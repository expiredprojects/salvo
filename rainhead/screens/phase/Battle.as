package rainhead.screens.phase 
{
	import rainhead.actors.Board;
	import rainhead.actors.Field;
	import rainhead.actors.Segment;
	import rainhead.actors.Ship;
	import rainhead.events.rps.Rolled;
	import rainhead.screens.GameScreen;
	import starling.display.Button;
	import starling.events.Event;
	import starling.events.EventDispatcher;
	
	public class Battle extends RainingPhase 
	{
		private var enemyBoard:Board;
		private var tempField:Field;
		private var bPlayerTurn:Boolean;
		private var turn:int = 0;
		
		public function Battle(_dad:EventDispatcher, _enemyBoard:Board) 
		{
			dad = _dad;
			mother = dad as GameScreen;
			enemyBoard = _enemyBoard;
		}
		
		public function beginPhase(rolled:String):void 
		{			
			if (rolled == Rolled.WON) 
			{
				bPlayerTurn = true;
			}
			else 
			{
				bPlayerTurn = false;
				SendToEnemyAndListen();
			}
			
			dad.addEventListener(Event.TRIGGERED, onTrigger);
		}
		
		public function endPhase():void 
		{
			turn = 0;
			dad.removeEventListener(Event.TRIGGERED, onTrigger);
		}
		
		private function onTrigger(e:Event):void 
		{
			var buttonClicked:Button = e.target as Button;
			if (buttonClicked.parent == enemyBoard && bPlayerTurn) 
			{
				bPlayerTurn = false;
				tempField = buttonClicked as Field;
				tempField.enabled = false;
				turn++;
				
				SendToEnemyAndListen();
			}
		}		
		
		private function SendToEnemyAndListen():void 
		{
			var ix:int = Math.random() * 9;
			var iy:int = Math.random() * 9;
			
			turn++;
			bPlayerTurn = true;
			trace(turn, " : ", ix, " : ", iy);
		}
		
		public function get isTurn():int 
		{
			return turn;
		}
	}
}