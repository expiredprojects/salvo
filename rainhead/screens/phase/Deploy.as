package rainhead.screens.phase 
{
	import rainhead.actors.Board;
	import rainhead.actors.Field;
	import rainhead.actors.Segment;
	import rainhead.actors.Ship;
	import rainhead.screens.GameScreen;
	import rainhead.utils.Atlas;
	import starling.display.Button;
	import starling.events.Event;
	import starling.events.EventDispatcher;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	
	public class Deploy extends RainingPhase
	{
		private var playerBoard:Board;
		private var touchX:Number;
		private var touchY:Number;
		private var bTick:Boolean;
		private var bTranslate:Boolean;
		private var bRotate:Boolean;
		private var tempShip:Ship;
		private var tempOrigin:Segment;
		private var lastOrigin:Segment;
		
		public function Deploy(_dad:EventDispatcher, _playerBoard:Board) 
		{
			dad = _dad;
			mother = dad as GameScreen;
			playerBoard = _playerBoard;
		}
		
		public function beginPhase():void 
		{
			clearTemp();
			clearBool();
			dad.addEventListener(Event.TRIGGERED, onTrigger);
		}
		
		public function endPhase():void 
		{
			dad.removeEventListener(Event.TRIGGERED, onTrigger);
		}
		
		private function onTouch(e:TouchEvent):void 
		{
			var touch:Touch = e.getTouch(mother.stage);
			if (touch != null) 
			{
				touchX = touch.globalX;
				touchY = touch.globalY;
			}
			
			if (!bTick) 
			{
				bTick = true;
				dad.addEventListener(Event.ENTER_FRAME, onTick);
			}
		}
		
		private function onTick():void 
		{
			if (tempShip == null) 
			{
				return;
			}
			else if (bTranslate) 
			{
				tempShip.x -= (tempShip.x - touchX);
				tempShip.y -= (tempShip.y - touchY);
			}
			else if (bRotate) 
			{
				tempShip.rotation = Math.atan2(touchY - tempShip.y, touchX - tempShip.x);
			}	
		}
		
		private function onTrigger(e:Event):void 
		{
			var buttonClicked:Button = e.target as Button;
			if ((buttonClicked as Button) is Segment) 
			{
				if (!bTranslate && !(bRotate && buttonClicked.parent != tempShip)) 
				{
					pickupSegment(buttonClicked as Segment);	
				}
			}
			else if ((buttonClicked as Button) is Field) 
			{
				if (bTranslate) 
				{
					translateSegment(buttonClicked as Field);
				}
				else if (bRotate && (buttonClicked as Field).status == Field.AVAILABLE) 
				{
					rotateSegment(buttonClicked as Field);
				}
			}
		}
		
		private function pickupSegment(buttonClicked:Segment):void 
		{			
			bTranslate = true;
			setTemp(buttonClicked);
			toggleOrigin(tempOrigin, false);
			
			if (tempOrigin.ix != 11 && tempOrigin.iy != 11) 
			{
				if (bRotate) 
				{
					searchMark();
				}	
				searchEdges();
				clearOrigins();
			}
			
			dad.addEventListener(TouchEvent.TOUCH, onTouch);
		}
		
		private function translateSegment(buttonClicked:Field):void 
		{
			bTranslate = false;
			translateTemp(buttonClicked);
			toggleOrigin(tempOrigin, true);
			
			if (tempShip.length > 1) 
			{
				toggleOrigin(lastOrigin, false);
				searchMark();
				bRotate = true;
			}
			else 
			{
				endTick();
				transitionValues(lastOrigin, buttonClicked);
				searchEdges();
				clearTemp();
			}
		}
		
		private function rotateSegment(buttonClicked:Field):void 
		{
			bRotate = false;
			endTick();
			
			searchMark();
			rotateTemp(buttonClicked);
			toggleOrigin(lastOrigin, true);
			
			searchEdges();
			unifyValues();
			clearTemp();
		}
		
		private function endTick():void 
		{
			dad.removeEventListener(TouchEvent.TOUCH, onTouch);
			dad.removeEventListener(Event.ENTER_FRAME, onTick);
			bTick = false;
		}
		
		private function clearBool():void 
		{
			bTick = false;
			bTranslate = false;
			bRotate = false;
		}
		
		private function clearTemp():void 
		{
			tempShip = null;
			tempOrigin = null;
			lastOrigin = null;
		}
		
		private function setTemp(buttonClicked:Segment):void 
		{
			tempShip = buttonClicked.parent as Ship;
			tempOrigin = tempShip.getChildAt(0) as Segment;
			lastOrigin = tempShip.getChildAt(tempShip.numChildren - 1) as Segment;
		}
		
		private function translateTemp(buttonClicked:Field):void 
		{
			tempShip.x = buttonClicked.parent.x + buttonClicked.x;
			tempShip.y = buttonClicked.parent.y + buttonClicked.y;
			transitionValues(tempOrigin, buttonClicked);
		}
		
		private function rotateTemp(buttonClicked:Field):void 
		{
			tempShip.rotation = Math.atan2(buttonClicked.parent.y + buttonClicked.y - tempShip.y, buttonClicked.parent.x + buttonClicked.x - tempShip.x);
			transitionValues(lastOrigin, buttonClicked);
		}
		
		private function clearOrigins():void 
		{
			tempOrigin.ix = 11;
			tempOrigin.iy = 11;
			lastOrigin.ix = 11;
			lastOrigin.iy = 11;
		}
		
		private function toggleOrigin(origin:Segment, bState:Boolean):void 
		{
			origin.enabled = bState;
			origin.touchable = bState;
		}
		
		private function transitionValues(origin:Segment, clicked:Field):void 
		{
			origin.ix = clicked.ix;
			origin.iy = clicked.iy;
		}
		
		private function searchEdges():void 
		{
			var i:int;
			var j:int;
			var tempEdge:Field;
			
			if ((tempOrigin.ix <= lastOrigin.ix && tempOrigin.iy == lastOrigin.iy) || (tempOrigin.ix == lastOrigin.ix && tempOrigin.iy <= lastOrigin.iy))
			{
				for (i = (tempOrigin.iy - 1); i <= (lastOrigin.iy + 1); i++) 
				{
					if (i < 0 || i > 9) 
					{
						continue;
					}
				
					for (j = (tempOrigin.ix - 1); j <= (lastOrigin.ix + 1); j++) 
					{
						if (j < 0 || j > 9)
						{
							continue;
						}
						
						tempEdge = playerBoard.arrayBoard[i][j] as Field;
						toggleEdge(tempEdge);
					}
				}
			}
			else
			{
				for (i = (tempOrigin.iy + 1); i >= (lastOrigin.iy - 1); i--) 
				{
					if (i < 0 || i > 9) 
					{
						continue;
					}
				
					for (j = (tempOrigin.ix + 1); j >= (lastOrigin.ix - 1); j--) 
					{
						if (j < 0 || j > 9) 
						{
							continue;
						}
						
						tempEdge = playerBoard.arrayBoard[i][j] as Field;
						toggleEdge(tempEdge);
					}
				}
			}
		}
		
		private function toggleEdge(tempEdge:Field):void 
		{
			var index:int = tempEdge.guests.indexOf(tempShip.length);
			if (index < 0) 
			{
				tempEdge.guests.push(tempShip.length);
				tempEdge.enabled = false;
			}
			else 
			{
				tempEdge.guests.splice(index, 1);
				if (tempEdge.guests.length == 0) 
				{
					tempEdge.enabled = true;
				}
			}
		}
		
		private function searchMark():void 
		{
			var plusX:int = tempOrigin.ix + tempShip.length - 1;
			var plusY:int = tempOrigin.iy + tempShip.length - 1;
			var minusX:int = tempOrigin.ix - tempShip.length + 1;
			var minusY:int = tempOrigin.iy - tempShip.length + 1;
			var tempMark:Field;
			
			if (plusX <= 9) 
			{
				tempMark = (playerBoard.arrayBoard[tempOrigin.iy][plusX] as Field);
				toggleMark(tempMark);
			}
			if (minusX >= 0) 
			{
				tempMark = (playerBoard.arrayBoard[tempOrigin.iy][minusX] as Field);
				toggleMark(tempMark);
			}
			if (plusY <= 9) 
			{
				tempMark = (playerBoard.arrayBoard[plusY][tempOrigin.ix] as Field);
				toggleMark(tempMark);
			}
			if (minusY >= 0) 
			{
				tempMark = (playerBoard.arrayBoard[minusY][tempOrigin.ix] as Field);
				toggleMark(tempMark);
			}
		}
		
		private function toggleMark(tempMark:Field):void 
		{
			var errors:int = errorCheck(tempMark, errors);
			
			if (tempMark.status == Field.UNKNOWN && tempMark.enabled && errors == 0) 
			{
				tempMark.setStatus(Field.AVAILABLE);
			}
			else 
			{
				tempMark.setStatus(Field.UNKNOWN);
			}
		}
		
		private function errorCheck(tempMark:Field, errors:int):int 
		{
			var i:int = 0;
			if (tempOrigin.iy == tempMark.iy) 
			{
				if (tempOrigin.ix < tempMark.ix) 
				{
					for (i = tempOrigin.ix; i <= tempMark.ix; i++) 
					{
						if ((playerBoard.arrayBoard[tempMark.iy][i] as Field).guests.length != 0) 
						{
							errors++;
						}
					}
				}
				else 
				{
					for (i = tempOrigin.ix; i >= tempMark.ix; i--) 
					{
						if ((playerBoard.arrayBoard[tempMark.iy][i] as Field).guests.length != 0) 
						{
							errors++;
						}
					}
				}
			}
			else 
			{
				if (tempOrigin.iy < tempMark.iy) 
				{
					for (i = tempOrigin.iy; i <= tempMark.iy; i++) 
					{
						if ((playerBoard.arrayBoard[i][tempMark.ix] as Field).guests.length != 0) 
						{
							errors++;
						}
					}
				}
				else 
				{
					for (i = tempOrigin.iy; i >= tempMark.iy; i--) 
					{
						if ((playerBoard.arrayBoard[i][tempMark.ix] as Field).guests.length != 0) 
						{
							errors++;
						}
					}
				}
			}
			
			return errors;
		}
		
		private function unifyValues():void 
		{
			if (tempShip.length < 3) 
			{
				return;
			}
			
			var i:int;
			var currentSegment:Segment;
			
			if (tempOrigin.ix <= lastOrigin.ix && tempOrigin.iy == lastOrigin.iy)
			{
				for (i = 1; i < tempShip.length - 1; i++) 
				{
					currentSegment = tempShip.getChildAt(i) as Segment;
					currentSegment.ix = tempOrigin.ix + i;
					currentSegment.iy = tempOrigin.iy;
				}
			}
			else if (tempOrigin.iy == lastOrigin.iy) 
			{
				for (i = tempShip.length - 2; i > 0; i--) 
				{
					currentSegment = tempShip.getChildAt(i) as Segment;
					currentSegment.ix = tempOrigin.ix - i;
					currentSegment.iy = tempOrigin.iy;
				}
			}
			else if (tempOrigin.ix == lastOrigin.ix && tempOrigin.iy <= lastOrigin.iy) 
			{
				for (i = 1; i < tempShip.length - 1; i++) 
				{
					currentSegment = tempShip.getChildAt(i) as Segment;
					currentSegment.ix = tempOrigin.ix;
					currentSegment.iy = tempOrigin.iy + i;
				}
			}
			else 
			{
				for (i = tempShip.length - 2; i > 0; i--) 
				{
					currentSegment = tempShip.getChildAt(i) as Segment;
					currentSegment.ix = tempOrigin.ix;
					currentSegment.iy = tempOrigin.iy - i;
				}
			}
		}
		
		public function get isTick():Boolean 
		{
			return bTick;
		}
	}
}