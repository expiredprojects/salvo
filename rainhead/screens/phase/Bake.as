package rainhead.screens.phase 
{
	import rainhead.actors.Board;
	import rainhead.actors.Field;
	import rainhead.actors.Segment;
	import rainhead.actors.Ship;
	
	public class Bake extends RainingPhase 
	{
		private var fleet:Array;
		private var playerBoard:Board;
		
		public function Bake(_playerBoard:Board, _fleet:Array) 
		{
			playerBoard = _playerBoard;
			fleet = _fleet;
		}
		
		public function runPhase():void 
		{
			bakeFleet();
		}
		
		private function bakeFleet():void 
		{
			var ix:int;
			var iy:int;
			
			for (var i:int = 0; i < fleet.length; i++) 
			{
				for (var j:int = 0; j < (fleet[i] as Ship).length; j++) 
				{
					ix = (fleet[i].getChildAt(j) as Segment).ix;
					iy = (fleet[i].getChildAt(j) as Segment).iy;
					
					(playerBoard.arrayBoard[iy][ix] as Field).setStatus(Segment.ALIVE);
				}
				
				fleet[i].visible = false;
			}
		}
	}
}