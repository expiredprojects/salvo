package rainhead.screens.phase 
{
	import rainhead.events.rps.Roll;
	import rainhead.ui.Mover;
	import starling.core.Starling;
	import starling.display.Quad;
	import starling.events.Event;
	
	public class RPS extends RainingPhase 
	{
		private var blackCanvas:Quad;
		private var rock:Mover;
		private var paper:Mover;
		private var scissors:Mover;
		
		public function RPS() 
		{
			this.addEventListener(Event.ADDED_TO_STAGE, initSelf);
		}
		
		private function initSelf(e:Event):void 
		{
			this.removeEventListener(Event.ADDED_TO_STAGE, initSelf);
			
			blackCanvas = new Quad(800, 600, 0x000000);
			blackCanvas.alpha = 0.5;
			addChild(blackCanvas);
			
			rock = new Mover(Roll.ROCK);
			addChild(rock);
			rock.x = ((0.6 * blackCanvas.width) - rock.width) >> 1;
			rock.y = 0.33 * blackCanvas.height;
			
			paper = new Mover(Roll.PAPER);
			addChild(paper);
			paper.x = rock.x + 0.4 * blackCanvas.width;
			paper.y = rock.y;
			
			scissors = new Mover(Roll.SCISSORS);
			addChild(scissors);
			scissors.x = (blackCanvas.width - scissors.width) >> 1;
			scissors.y = 0.66 * blackCanvas.height;
		}
		
		public function beginPhase():void 
		{
			this.visible = true;
			this.addEventListener(Event.TRIGGERED, onTrigger);
		}
		
		public function endPhase():void 
		{
			this.removeEventListener(Event.TRIGGERED, onTrigger);
			this.visible = false;
		}
		
		private function onTrigger(e:Event):void 
		{
			var buttonClicked:Mover = e.target as Mover;
			
			disableRPS();
			if (buttonClicked == rock) 
			{
				this.dispatchEvent(new Roll(Roll.ROCK));
			}
			else if (buttonClicked == paper) 
			{
				this.dispatchEvent(new Roll(Roll.PAPER));
			}
			else if (buttonClicked == scissors) 
			{
				this.dispatchEvent(new Roll(Roll.SCISSORS));
			}
		}
		
		private function disableRPS():void 
		{
			rock.enabled = false;
			paper.enabled = false;
			scissors.enabled = false;
		}
		
		public function restartRPS():void 
		{
			rock.enabled = true;
			paper.enabled = true;
			scissors.enabled = true;
		}
	}
}