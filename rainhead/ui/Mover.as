package rainhead.ui 
{
	import rainhead.utils.Atlas;
	import starling.display.Button;
	
	public class Mover extends Button 
	{
		public static const LOGIN:String = "login";
		public static const LOGOUT:String = "logout";
		public static const ABORT:String = "abort";
		public static const READY:String = "ready";
		
		public function Mover(type:String) 
		{
			super(Atlas.getTexture(type));
		}
	}
}