package rainhead.ui 
{	
	import feathers.controls.TextInput;
	import feathers.display.Scale9Image;
	import feathers.textures.Scale9Textures;
	import flash.geom.Rectangle;
	import rainhead.utils.Atlas;
	import starling.events.Event;
	
	public class Typo extends TextInput
	{
		public static const LOGIN:String = "typo_login";
		private var mover:Mover;
		
		public function Typo(w:int, h:int, m:Mover) 
		{
			this.width = w;
			this.height = h;
			this.backgroundSkin = new Scale9Image(new Scale9Textures(Atlas.getTexture(LOGIN), new Rectangle(w * 0.1, h * 0.1, w * 0.8, h * 0.8)));
			mover = m;
		}	
		
		public function disposeTypo():void 
		{
			removeEventListener("enter", onKey);
			this.hideFocus();
			this.visible = false;
		}
		
		public function initTypo():void 
		{
			this.visible = true;
			this.setFocus();
			addEventListener("enter", onKey);
		}
		
		private function onKey(e:Event):void 
		{
			mover.dispatchEvent(new Event(Event.TRIGGERED, true));
		}
	}
}