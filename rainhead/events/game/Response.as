package rainhead.events.game 
{	
	import rainhead.events.RainingEvent;
	
	public class Response extends RainingEvent 
	{
		public static const RESPONSE:String = "response";
		
		public static const OPPONENT_READY:String = "opponent_ready";
		public static const ABORT_SUCCESS:String = "abort_success"; 
		
		public function Response(header:String) 
		{
			super(RESPONSE, true, header);
		}
	}
}