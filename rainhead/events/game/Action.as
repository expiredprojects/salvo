package rainhead.events.game 
{
	import rainhead.events.RainingEvent;
	
	public class Action extends RainingEvent 
	{
		public static const ACTION:String = "action";
		
		public static const ABORT:String = "abort";
		public static const READY:String = "ready";
		
		public function Action(header:String) 
		{
			super(ACTION, true, header);
		}
	}
}