package rainhead.events.lobby 
{
	import rainhead.events.RainingEvent;
	
	public class Request extends RainingEvent 
	{
		public static const REQUEST:String = "request";
		
		public static const REQUEST_MATCH:String = "request_match";
		public static const REQUEST_FRIEND:String = "request_friend";
		
		public function Request(header:String) 
		{
			super(REQUEST, true, header);
		}
		
	}

}