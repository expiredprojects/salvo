package rainhead.events 
{	
	import starling.events.Event;
	
	public class RainingEvent extends Event
	{
		public function RainingEvent(_type:String, _bubbles:Boolean = false, _data:Object = null) 
		{
			super(_type, _bubbles, _data);
		}
		
		public function get result():Object 
		{
			return data;
		}
	}
}