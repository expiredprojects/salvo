package rainhead.events.rps 
{
	import rainhead.events.RainingEvent;
	
	public class Roll extends RainingEvent 
	{
		public static const ROLL:String = "roll";
		
		public static const ROCK:String = "rock";
		public static const PAPER:String = "paper";
		public static const SCISSORS:String = "scissors";
		
		public function Roll(header:String) 
		{
			super(ROLL, true, header);
		}
	}
}