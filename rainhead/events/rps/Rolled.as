package rainhead.events.rps 
{
	import rainhead.events.RainingEvent;
	
	public class Rolled extends RainingEvent 
	{
		public static const ROLLED:String = "rolled";
		
		public static const DRAW:String = "draw";
		public static const LOST:String = "lost";
		public static const WON:String = "won";
		
		public function Rolled(header:String) 
		{
			super(ROLLED, true, header);
		}
	}
}