package rainhead.events.battle 
{
	import rainhead.events.RainingEvent;
	import rainhead.utils.Location;
	
	public class Shoot extends RainingEvent 
	{
		public static const SHOOT = "shoot";
		
		public function Shoot(header:Location) 
		{
			super(SHOOT, true, header);
		}
	}
}