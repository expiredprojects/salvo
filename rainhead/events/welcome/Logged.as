package rainhead.events.welcome 
{
	import rainhead.events.RainingEvent;
	
	public class Logged extends RainingEvent 
	{
		public static const LOGGED:String = "logged";
		
		public static const ONLINE:Boolean = true;
		public static const OFFLINE:Boolean = false;
		
		public function Logged(header:Boolean) 
		{
			super(LOGGED, true, header);
		}
	}
}