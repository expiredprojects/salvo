package rainhead.events.welcome 
{
	import rainhead.events.RainingEvent;
	
	public class Login extends RainingEvent 
	{
		public static const LOGIN:String = "login";
		
		public function Login(header:String) 
		{
			super(LOGIN, true, header);
		}
	}
}